<?php

  include_once('includes/database.php');

  // Create header for JSON
  header('Content-Type: application/json; charset=utf8');

  // Filter all fields

  $custnr = filter_input(INPUT_POST, 'custnr', FILTER_VALIDATE_INT);
  $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
  $address = filter_input(INPUT_POST, 'address', FILTER_SANITIZE_STRING);
  $zip = filter_input(INPUT_POST, 'zip', FILTER_SANITIZE_STRING);
  $city = filter_input(INPUT_POST, 'city', FILTER_SANITIZE_STRING);
  $phone = filter_input(INPUT_POST, 'phone', FILTER_SANITIZE_STRING);
  $credit = filter_input(INPUT_POST, 'credit', FILTER_SANITIZE_STRING);
  $action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING);

  if (empty($name) || empty($custnr) || empty($address) || empty($zip) || empty($city) || empty($phone) || $credit) {
    $msg;
  }

  // SQL for inserting client

    if ($action == 'edit') {
        $query = "UPDATE klanten
                  SET Naam = :name, Adres = :address, Postcode = :zip, Plaats = :city, Telefoon =:phone, Kredietcode =:credit
                  WHERE Klantnr = :custnr;";
        $statement = $pdo->prepare($query);
        $statement->bindValue('custnr', $custnr, PDO::PARAM_INT);
        $statement->bindValue('name', $name, PDO::PARAM_STR);
        $statement->bindValue('address', $address, PDO::PARAM_STR);
        $statement->bindValue('zip', $zip, PDO::PARAM_STR);
        $statement->bindValue('city', $city, PDO::PARAM_STR);
        $statement->bindValue('phone', $phone, PDO::PARAM_STR);
        $statement->bindValue('credit', $credit, PDO::PARAM_STR);
    } else if($action == 'delete') {
        $query = "DELETE FROM klanten WHERE Klantnr = :custnr";
        $statement = $pdo->prepare($query);
        $statement->bindValue('custnr', $custnr, PDO::PARAM_INT);

    } else {
        $query = "INSERT INTO klanten (Klantnr, Naam, Adres, Postcode, Plaats, Telefoon, Kredietcode)
                  VALUES (:custnr, :name, :address, :zip, :city, :phone, :credit)";
        $statement = $pdo->prepare($query);
        $statement->bindValue('custnr', $custnr, PDO::PARAM_INT);
        $statement->bindValue('name', $name, PDO::PARAM_STR);
        $statement->bindValue('address', $address, PDO::PARAM_STR);
        $statement->bindValue('zip', $zip, PDO::PARAM_STR);
        $statement->bindValue('city', $city, PDO::PARAM_STR);
        $statement->bindValue('phone', $phone, PDO::PARAM_STR);
        $statement->bindValue('credit', $credit, PDO::PARAM_STR);
    }


    $ok = $statement->execute();
    $count = $statement->rowCount();
    $msg = "Inserted $count row(s).\n";
    if (!$ok) {
      echo "SQL Error: ".$statement->errorInfo()[2];
    }

    json_encode($msg);
  unset($pdo);
