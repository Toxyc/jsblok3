<?php

  include_once('includes/database.php');

  // Create header for JSON

  header('Content-Type: application/json; charset=utf8');
  $name = filter_input(INPUT_GET, 'name', FILTER_SANITIZE_STRING);

  // Functions for getting database items

    $query = "SELECT * FROM klanten WHERE naam LIKE :name";
    $statement = $pdo->prepare($query);
    $statement->bindValue('name', '%'.$name.'%', PDO::PARAM_STR);
    $ok = $statement->execute();
    $results = $statement->fetchAll(PDO::FETCH_ASSOC);
    $clients = json_encode($results);
    echo $clients;

  unset($pdo);
