'use strict';

var lat, currentPos, lng, dest, map, directionsService, directionsDisplay, marker;

window.onload = getClients;

document.querySelector('#clientName').addEventListener("keyup", function(ev){
    ev.preventDefault();
    var clientName = document.querySelector('#clientName').value;
    searchClient(clientName);
});

document.querySelector('#insert').addEventListener("click", function(ev){
    ev.preventDefault();
    insertClient();
});

document.querySelector('#edit').addEventListener("click", function(ev){
    ev.preventDefault();
    editClient();
});

document.querySelector('#delete').addEventListener("click", function(ev){
    ev.preventDefault();
    deleteClient();
});


function getClients() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var clients = this.responseText;
      clients = JSON.parse(clients);
      var table = document.querySelector('#clients');
      var tbody = document.querySelector('tbody');
      while(tbody.firstChild) {
        tbody.removeChild(tbody.firstChild);
      }
      for (var i = 0; i < clients.length; i++) {
        var tr = document.createElement("tr");
          for (var key in clients[i]) {
            var td = document.createElement("td");
            td.innerHTML = clients[i][key];
            tr.appendChild(td);
          }
        tbody.appendChild(tr);
      }
      rowHandler();
    }
  };
  xhttp.open("GET", "getData.php", true);
  xhttp.send();
}

function rowHandler() {
    var trows = document.querySelectorAll('tr');
    for (var i = 0; i < trows.length; i++) {
        trows[i].addEventListener('click', function (ev) {
            ev.preventDefault();
            var address = this.childNodes[2].childNodes[0].nodeValue;
            var city = this.childNodes[4].childNodes[0].nodeValue;
            getClientLoc(address, city);
            var custnr = document.querySelector('#editCustnr');
            var name = document.querySelector('#editName');
            var address = document.querySelector('#editAddress');
            var zip = document.querySelector('#editZip');
            var city = document.querySelector('#editCity');
            var phone = document.querySelector('#editPhone');
            var credit = document.querySelector('#editCredit');
            for (var j = 0; j < this.childNodes.length; j++) {
              console.log(this.childNodes[j].childNodes[0].nodeValue);
              custnr.value = this.childNodes[0].innerHTML;
              name.value = this.childNodes[1].innerHTML;
              address.value = this.childNodes[2].innerHTML;
              zip.value = this.childNodes[3].innerHTML;
              city.value = this.childNodes[4].innerHTML;
              phone.value = this.childNodes[5].innerHTML;
              credit.value = this.childNodes[6].innerHTML;
            }
        });
    }
}

function searchClient(clientName) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var clients = this.responseText;
      clients = JSON.parse(clients);
      var table = document.querySelector('#clients');
      var tbody = document.querySelector('tbody');
      while(tbody.firstChild) {
        tbody.removeChild(tbody.firstChild);
      }
      for (var i = 0; i < clients.length; i++) {
        var tr = document.createElement("tr");
          for (var key in clients[i]) {
            var td = document.createElement("td");
            td.innerHTML = clients[i][key];
            tr.appendChild(td);
          }
        tbody.appendChild(tr);
      }
    }
  };
  xhttp.open("GET", "getData.php?name="+clientName, true);
  xhttp.send();
}


function insertClient() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var msg = this.responseText;
      var msgDiv = document.querySelector('.msg');
      msgDiv.innerHTML = msg;
      getClients();
    }
  };
  xhttp.open("POST", "processData.php?action=insert", true);
  var form = document.querySelector('#addForm');
  var formData = new FormData(form);
  xhttp.send(formData);
}

function editClient() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var msg = this.responseText;
            var msgDiv = document.querySelector('.msg');
            msgDiv.innerHTML = msg;
            getClients();
        }
    };
    xhttp.open("POST", "processData.php?action=edit", true);
    var form = document.querySelector('#addForm');
    var formData = new FormData(form);
    xhttp.send(formData);
}

function deleteClient() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var msg = this.responseText;
            var msgDiv = document.querySelector('.msg');
            msgDiv.innerHTML = msg;
            getClients();
        }
    };
    xhttp.open("POST", "processData.php?action=delete", true);
    var form = document.querySelector('#addForm');
    var formData = new FormData(form);
    xhttp.send(formData);
}

// Google maps
// API KEY: AIzaSyAfQGEQ4Q5rO04Sk69zZPhnxOutRWcyd48

navigator.geolocation.getCurrentPosition(getLoc);

function getLoc(position) {
    lat = position.coords.latitude;
    lng = position.coords.longitude;
    currentPos = {lat: lat, lng: lng};
}

function getClientLoc(address, city) {
    address =  address + ", " + city;
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode( { 'address': address}, function(results, status) {

        if (status === 'OK') {
          dest = results[0].geometry.location;
            console.log(dest);
            map.setCenter(dest);
            var marker = new google.maps.Marker({
                position: dest,
                map: map
            });
            marker.setMap(null);
            Route();
        }
    });
}

function initMap() {
    var pos = {lat: 53.207176, lng: 6.556771};
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        center: pos
    });
    marker = new google.maps.Marker({
        position: pos,
        map: map
    });
    directionsService = new google.maps.DirectionsService;
    directionsDisplay = new google.maps.DirectionsRenderer({
        map: map
    });
}

function Route() {
    if (currentPos) {
        marker.setMap(null);
        marker = new google.maps.Marker({
            position: currentPos,
            map: map
        });
    }
    directionsService.route({
        origin: currentPos,
        destination: dest,
        travelMode: google.maps.TravelMode.DRIVING
    }, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        } else {
            window.alert('Directions request failed due to ' + status);
        }
    });
}
